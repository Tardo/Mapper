/* (c) Magnus Auvinen. See licence.txt in the root of the distribution for more information. */
/* If you are missing that file, acquire a complete release at teeworlds.com.                */
#include "mapper.h"
#include <engine/storage.h>
#include <engine/config.h>
#include <engine/server.h>
#include <engine/map.h>
#include <engine/shared/datafile.h>
#include <base/system.h>
#include "../entities/pickup.h"
#include "../gamecontext.h"

CGameControllerMapper::CGameControllerMapper(class CGameContext *pGameServer)
: IGameController(pGameServer)
{
	m_pGameType = "Mapper";
}

bool CGameControllerMapper::OnEntity(int Index, vec2 Pos)
{
	return IGameController::OnEntity(Index, Pos);
}

void CGameControllerMapper::OnMessage(int MsgID, void *pRawPacket, int ClientID)
{
    switch (MsgID)
    {
        case NETMSGTYPE_SVAN_TILEMODIF:
        {
            CNetMsg_SvAn_TileModif *packetTileModif = (CNetMsg_SvAn_TileModif*)pRawPacket;

            // check if game layer modif
            CMapItemLayer *pLayer = GameServer()->Collision()->Layers()->GetLayer(packetTileModif->m_Layer);
            CMapItemLayerTilemap *pTilemap = reinterpret_cast<CMapItemLayerTilemap *>(pLayer);
            if (pTilemap == GameServer()->Collision()->Layers()->GameLayer())
            {
                CheckEntities(vec2(packetTileModif->m_X, packetTileModif->m_Y)); // If modif in exists entity, remove it
                if (packetTileModif->m_Index > ENTITY_OFFSET)
                    OnEntity(packetTileModif->m_Index-ENTITY_OFFSET, vec2(packetTileModif->m_X*32.0f + 16.0f, packetTileModif->m_Y*32.0f + 16.0f)); // create new entity
            }

            GameServer()->Collision()->CreateTile(vec2(packetTileModif->m_X, packetTileModif->m_Y), packetTileModif->m_Group, packetTileModif->m_Layer, packetTileModif->m_Index, packetTileModif->m_Flags);

            // Send Creation to All Clients (TW)
            CNetMsg_SvAn_TileModif Msg;
            Msg.m_X = packetTileModif->m_X;
            Msg.m_Y = packetTileModif->m_Y;
            Msg.m_Group = packetTileModif->m_Group;
            Msg.m_Layer = packetTileModif->m_Layer;
            Msg.m_Index = packetTileModif->m_Index;
            Msg.m_Flags = packetTileModif->m_Flags;
            Server()->SendPackMsg(&Msg, MSGFLAG_VITAL|MSGFLAG_FLUSH, -1);
        } break;

        default:
            IGameController::OnMessage(MsgID, pRawPacket, ClientID);
    }
}

void CGameControllerMapper::CheckEntities(vec2 pos)
{
    CPickup *aEnts[1];
    if (GameServer()->m_World.FindEntities(vec2(pos.x*32.0f + 16.0f, pos.y*32.0f + 16.0f), 0.0f, (CEntity**)aEnts, 1, CGameWorld::ENTTYPE_PICKUP))
        GameServer()->m_World.DestroyEntity(aEnts[0]);
}

void CGameControllerMapper::Tick()
{
	IGameController::Tick();
}
