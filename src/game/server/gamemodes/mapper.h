/* (c) Magnus Auvinen. See licence.txt in the root of the distribution for more information. */
/* If you are missing that file, acquire a complete release at teeworlds.com.                */
#ifndef GAME_SERVER_GAMEMODES_MAPPER_H
#define GAME_SERVER_GAMEMODES_MAPPER_H
#include <game/server/gamecontroller.h>

class CGameControllerMapper : public IGameController
{
public:
	CGameControllerMapper(class CGameContext *pGameServer);
	virtual void Tick();

	virtual void OnMessage(int MsgID, void *pRawPacket, int ClientID);
	virtual bool OnEntity(int Index, vec2 Pos);

private:
    void CheckEntities(vec2 pos);
};
#endif
